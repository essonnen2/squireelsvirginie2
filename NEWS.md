# squireelsvirginie2 0.0.0.9000
- add check_primary_color_is_ok()
- Add get_message_fur_color()
* Added a `NEWS.md` file to track changes to the package.
